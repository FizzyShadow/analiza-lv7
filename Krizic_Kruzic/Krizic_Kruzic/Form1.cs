﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Krizic_Kruzic
{
    public partial class Form1 : Form
    {
        protected Graphics g;
        GFX eng;
        Ploca ploca;
        public Form1()
        {
            InitializeComponent();
            
        }

        

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics u = panel1.CreateGraphics();
            eng = new GFX(u);

            ploca = new Ploca();
            ploca.initPloca();

            refresh();


        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Point mouse = Cursor.Position;
            mouse = panel1.PointToClient(mouse);

            ploca.detectHit(mouse);
            refresh();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Player1.Text = Interaction.InputBox("", "PLAYER1 NAME", "", 200, 200)+ ": ";
            Player2.Text = Interaction.InputBox("", "PLAYER2 NAME", "", 200, 200) + ": ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ploca.reset();
            GFX.setup();
        }

        public void refresh()
        {
            label1.Text = ploca.Count1.ToString();
            label3.Text = ploca.Count2.ToString();
        }
    }


}
