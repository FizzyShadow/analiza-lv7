﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Krizic_Kruzic
{
    class GFX
    {
        private static Graphics gObj;
        public GFX(Graphics g)
        {
            gObj = g;
            setup();
        }
        public static void setup()
        {
            Brush bg = new SolidBrush(Color.WhiteSmoke);
            Pen p = new Pen(Color.Black);

            gObj.FillRectangle(bg, 0, 0, 392, 392);
            gObj.DrawLine(p, 131, 0, 131, 392);
            gObj.DrawLine(p, 262, 0, 262, 392);
            gObj.DrawLine(p, 0, 131, 392, 131);
            gObj.DrawLine(p, 0, 262, 392, 262);


        }
        public static void drawX(Point loc)
        {
            Pen pen = new Pen(Color.Red, 1.5F);
            int xA = loc.X * 130;
            int yA = loc.Y * 130;
            gObj.DrawLine(pen, xA+20,yA+20,xA+110,yA+110);
            gObj.DrawLine(pen, xA+110, yA+20, xA+20, yA + 110);
        }

        public static void drawO(Point loc)
        {
            Pen pen = new Pen(Color.Red, 1.5F);
            int xA = loc.X * 130;
            int yA = loc.Y * 130;
            gObj.DrawEllipse(pen, xA+10, yA+10, 110,110);
            
        }
    }
}
