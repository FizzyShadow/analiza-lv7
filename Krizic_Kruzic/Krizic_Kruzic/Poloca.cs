﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Krizic_Kruzic
{
    class Ploca
    {

        private Holder[,] holders = new Holder[3, 3];
        public int potez = 0;
        private int count1 = 0;
        private int count2 = 0;


        public const int X = 0;
        public const int Y = 1;
        public const int B = 2;

        public int Count1
        {
            get
            {
                return count1;
            }
            set
            {
                count1 = value;
            }
        }
        public int Count2
        {
            get
            {
                return count2;
            }
            set
            {
                count2 = value;
            }
        }
        public void initPloca()
        {

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {

                    holders[x, y] = new Holder();
                    holders[x, y].Vrijednost = B;
                    holders[x, y].Lokacija = new Point(x, y);
                }
            }
        }
        public void detectHit(Point loc)
        {
            if (loc.Y < 392)
            {
                int x = 0;
                int y = 0;
                if (loc.X < 131)
                {
                    x = 0;
                }
                else if (loc.X > 131 && loc.X < 262)
                {
                    x = 1;
                }
                else
                {
                    x = 2;
                }
                if (loc.Y < 131)
                {
                    y = 0;
                }
                else if (loc.Y > 131 && loc.Y < 262)
                {
                    y = 1;
                }
                else if (loc.Y < 392)
                {
                    y = 2;
                }
               
                if (potez % 2 == 0)
                {
                    GFX.drawX(new Point(x, y));
                    holders[x, y].Vrijednost = X;
                    potez++;
                    if (Pobjeda(X))
                    {
                        count1++;
                        reset();
                        GFX.setup();
                        
                        
                    }
                }
                else
                {
                    GFX.drawO(new Point(x, y));
                    holders[x, y].Vrijednost = Y;
                    potez++;
                    if (Pobjeda(Y))
                    {
                        count2++;
                        reset();
                        GFX.setup();
                        
                    }
                    
                }
                
                if (potez == 9)
                {
                    reset();
                    GFX.setup();
                    
                }

            }
        }
        public bool Pobjeda(int X)
        {
            bool win = false;
            int x;
            for (x = 0; x < 3; x++)
            {
                if (holders[x, 0].Vrijednost == X && holders[x, 1].Vrijednost == X && holders[x, 2].Vrijednost == X) return true;
                else if (holders[0, x].Vrijednost == X && holders[1, x].Vrijednost == X && holders[2, x].Vrijednost == X) return true;
            }
            x = 0;
            if (holders[x, x].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x + 2].Vrijednost == X) return true;
            if (holders[x, x + 2].Vrijednost == X && holders[x + 1, x + 1].Vrijednost == X && holders[x + 2, x].Vrijednost == X) return true;

            return win;
        }

        public void reset()
        {
            holders = new Holder[3,3];
            initPloca();
            potez = 0;
        }
    }



        class Holder
    {
        
        private Point lokacija;
        private int vrijednost = Ploca.B;
        public Point Lokacija
        {
            get
            {
                return lokacija;
            }
            set
            {
                lokacija = value;
            }
        }

        public int Vrijednost
        {
            get
            {
                return vrijednost;
            }
            set
            {
                vrijednost = value;
            }
        }

       
        
        

    }
}
